# R-FCN and Soft-NMS G

Research of R-FCN (Region-based FCN Fully Convolutional Networks https://arxiv.org/abs/1605.06409) and Soft-NMS algorithm (https://arxiv.org/abs/1704.04503), being state-of-the-art convolutional models and method for object detection, respectively.

Project duration: Feb 2021 - July 2021.